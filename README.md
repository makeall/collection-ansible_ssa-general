# Ansible SSA Collection

This collection is build from several roles in the [Ansible SSA GitLab Group](https://gitlab.com/ansible-ssa).

## Build

There is a Gitlab CI/CD pipeline which automatically builds this collection.

1. Update all git sub modules.

```bash
git submodule foreach git pull
```

1. Make sure to update `ansible_ssa/general/galaxy.yml` and commit to master or release branch to trigger a new build.

1. Commit your changes

```bash
git commit -m "provide some reasonable description" -a
```

## Release process

To build a new release follow these instructions:

- update [ansible_ssa/general/galaxy.yml](ansible_ssa/general/galaxy.yml)

- set [.z-release](.z-release) to "0"

- push to GitLab

If building a new stable release, create a release tag as well. Also update the dependencies in the execution environment.

Make sure unstable releases have a higher release number than stable releases.
